import firebase from 'firebase/app'
import 'firebase/app'
import 'firebase/database'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyBp86BtzjP94-kj5gfj6S5jNJAXCT3KsGY",
    authDomain: "workshop-dv-441ab.firebaseapp.com",
    databaseURL: "https://workshop-dv-441ab.firebaseio.com/",
    projectId: "workshop-dv-441ab",
    storageBucket: "gs://workshop-dv-441ab.appspot.com/",
    messagingSenderId: "1989397051363264"
}

firebase.initializeApp(config)

const database = firebase.database()
const auth = firebase.auth()
const provider = new firebase.auth.FacebookAuthProvider()

export {
    database,
    auth,
    provider
}